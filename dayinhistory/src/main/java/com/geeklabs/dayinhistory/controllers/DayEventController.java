package com.geeklabs.dayinhistory.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.dayinhistory.Message;
import com.geeklabs.dayinhistory.domain.enums.Category;
import com.geeklabs.dayinhistory.dto.DayEventDto;
import com.geeklabs.dayinhistory.dto.InputDto;
import com.geeklabs.dayinhistory.service.DayEventService;
import com.geeklabs.dayinhistory.util.RequestMapper;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.ADMIN)
public class DayEventController {

	@Autowired
	private DayEventService dayEventService;
	/*************************************(((((((  Web )))))))))))**********************************************/
	@RequestMapping(value = RequestMapper.OPEN_DAY_EVENT, method = RequestMethod.GET)
	public String openDayEventPage() {
		return "dayevent";
	}

	@RequestMapping(value = RequestMapper.GET_ALL_DAY_EVENTS, method = RequestMethod.GET)
	public @ResponseBody List<DayEventDto> getAllDayEventDtos() {
		return dayEventService.getAllDayEventDtos();
	}
	
	
	@RequestMapping(value = RequestMapper.ADD_DAY_EVENT, method = RequestMethod.GET)
	public String addDayEvent(Model map) {
		DayEventDto dayEventDto = new DayEventDto();
		map.addAttribute("dayEvent", dayEventDto);

		mapAttributesToModel(map);

		return "adddayevent";
	}

	private void mapAttributesToModel(Model map) {
		// Send list of categories to front end
		List<String> categories = Category.getList();
		
		map.addAttribute("categories", categories);
		
		// Send list of cities to front end
		List<String> cityList = new ArrayList<>();
		cityList.add("Nizamabad, Telangana, India");
		cityList.add("Hyderabad, Telangana, India");
		cityList.add("Mahabubnagar, Telangana, India");
		
		map.addAttribute("cities", cityList);
	}
	
	@RequestMapping(value = RequestMapper.ADD_DAY_EVENT, method = RequestMethod.POST)
	public String addDayEvent(@ModelAttribute("dayEvent") DayEventDto dayEventDto, BindingResult result, Principal principal,
			final RedirectAttributes redirectAttributes, HttpServletRequest request, Model map ) {
		
		if (dayEventDto.getId() == null) {
			// save 
			ResponseStatus responseStatus = dayEventService.addEvent(dayEventDto);

			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Event saved successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Event alrady exist", Message.ERROR));
			}
			
		} else {
			// update
			ResponseStatus responseStatus = dayEventService.updateEvent(dayEventDto);

			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg", new Message("Event Updated successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg", new Message("Error occured while updating the Event.", Message.ERROR));
			}
		}
		return "redirect:/admin/dayevent";
	}
	
	@RequestMapping(value = RequestMapper.GET_EVENT_BY_ID, method = RequestMethod.GET)
	public String get(Model map, @PathVariable Long dayEventId) {
		DayEventDto dayEventDto = dayEventService.getDayEventById(dayEventId);
		map.addAttribute("dayEvent", dayEventDto);
		mapAttributesToModel(map);
		return "adddayevent";
	}
	
	
	@RequestMapping(value = RequestMapper.DELETE_DAY_EVENT, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus deleteDayEvent(@PathVariable Long dayEventId) {
		return dayEventService.deleteDayEvent(dayEventId);
	}
	
	/*************************************(((((((  Mobile )))))))))))**********************************************/
	@RequestMapping(value = RequestMapper.GET_DAY_EVENTS_BY_DATE_AND_CATEGORY, method = RequestMethod.POST)
	public @ResponseBody List<DayEventDto> getAllDayEventsByPlaceCategoryAndDate(@RequestParam int currentPage, @RequestBody InputDto inputDto) {
		return dayEventService.getAllDayEventsByPlaceCategoryAndDate(currentPage, inputDto);
	}
	
	@RequestMapping(value = RequestMapper.DAY_EVENT_LIKE_COUNT, method = RequestMethod.POST)
	public @ResponseBody ResponseStatus dayEventLikeCount(@PathVariable Long dayEventID) {
		return dayEventService.dayEventLikeCount(dayEventID);
	}
}

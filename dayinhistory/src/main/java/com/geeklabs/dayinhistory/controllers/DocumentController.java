package com.geeklabs.dayinhistory.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.geeklabs.dayinhistory.domain.Document;
import com.geeklabs.dayinhistory.service.DocumentService;
import com.geeklabs.dayinhistory.util.RequestMapper;

@Controller
@RequestMapping(value = RequestMapper.DOCUMENT)
public class DocumentController {

	@Autowired
	private DocumentService documentService;

	@RequestMapping(value = RequestMapper.GET_DOCUMENT, method = RequestMethod.GET)
	public ResponseEntity<byte[]> getDoc(@PathVariable String identifier) {
		Document doc = documentService.getDocument(identifier);

		final HttpHeaders headers = new HttpHeaders();
		if (doc != null) {
			headers.setContentType(MediaType.valueOf(doc.getMimeType()));

			return new ResponseEntity<byte[]>(doc.getContent(), headers, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<byte[]>(new byte[0], headers, HttpStatus.NO_CONTENT);
		}
	}
}
package com.geeklabs.dayinhistory.controllers;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.dayinhistory.domain.CustomUserDetails;
import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.dto.ChangePwdDto;
import com.geeklabs.dayinhistory.dto.ForgotPasswordDto;
import com.geeklabs.dayinhistory.service.UserService;
import com.geeklabs.dayinhistory.util.RequestMapper;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.USER)
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = RequestMapper.USER_LOGIN_SUCCESS, method = RequestMethod.GET)
	public String loginSuccess(Principal principal) {
		return "dayevent";
	}

	@RequestMapping(value = RequestMapper.USER_LOGIN_ERROR, method = RequestMethod.GET)
	public String loginFailure(Model map, RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("loginerrpr", true);
		return "redirect:/";
	}

	@RequestMapping(value = RequestMapper.SETUP, method = RequestMethod.GET)
	public String setup() {
		userService.setupAppUserRoles();
		userService.setupAppUser();
		return "login";
	}

	@RequestMapping(value = RequestMapper.MY_ACCOUNT, method = RequestMethod.GET)
	public String getMyAccount(Principal principal) {

		if (principal == null) {
			return "redirect:/";
		}
		return "myacount";
	}

	@RequestMapping(value = RequestMapper.MY_ACCOUNT, method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody
	ResponseStatus changePwd(Principal principal, @ModelAttribute("changePwd") ChangePwdDto changePwdDto) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		return userService.changePwd(changePwdDto, userDetails);
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.MY_ACCOUNT_GET_USER, method = RequestMethod.GET)
	public User getUser(Principal principal) {
		CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		User user = userService.getUserByEamailOrUserName(userDetails.getUsername(), userDetails.getEmail());
		return user;
	}
	
	@RequestMapping(value = RequestMapper.USER_FORGOTPASSWORD, method = RequestMethod.GET)
	public String getForgotPassword(Model map) {
		map.addAttribute("forgotPassword", new ForgotPasswordDto());
		return "forgotpassword";
	}
	
	@RequestMapping(value = RequestMapper.USER_FORGOTPASSWORD, method = RequestMethod.POST)
	public String sendForgotPasswordToMail(@ModelAttribute  ForgotPasswordDto forgotPasswordDto, RedirectAttributes redirectAttributes) {
			ResponseStatus responseStatus = userService.sendforgotPasswordToUserMail(forgotPasswordDto);
			if (responseStatus != null && responseStatus.getStatus().equals("success")) {
				redirectAttributes.addFlashAttribute("success", true);
				return "redirect:/";
			}else {
				redirectAttributes.addFlashAttribute("error", true);
				return "redirect:/";
			}
	}
}
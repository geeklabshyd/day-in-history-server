package com.geeklabs.dayinhistory.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.dayinhistory.domain.enums.Category;
import com.geeklabs.dayinhistory.dto.NewDayEventDto;
import com.geeklabs.dayinhistory.service.NewDayEventService;
import com.geeklabs.dayinhistory.util.RequestMapper;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.ADMIN)
public class NewDayEventController {

	@Autowired
	private NewDayEventService newDayEventService;
	
	/*************************************(((((((  Mobile )))))))))))**********************************************/
	@ResponseBody
	@RequestMapping(value = RequestMapper.ADD_DAY_EVENT_FROM_USER, method = RequestMethod.POST)
	public ResponseStatus addDayEvent(@RequestBody NewDayEventDto newDayEventDto) {
		return newDayEventService.AddEvent(newDayEventDto);

	}
	
	/*************************************(((((((  Web )))))))))))**********************************************/
	@RequestMapping(value = RequestMapper.OPEN_NEW_DAY_EVENT, method = RequestMethod.GET)
	public String openNewDayEventPage() {
		return "newdayevent";
	}
	
	@RequestMapping(value = RequestMapper.GET_ALL_NEW_DAY_EVENTS, method = RequestMethod.GET)
	public @ResponseBody List<NewDayEventDto> getAllNewDayEventDtos() {
		return newDayEventService.getAllNewDayEventDtos();
	}
	
	@RequestMapping(value = RequestMapper.DELETE_NEW_DAY_EVENT, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus deleteNewDayEvent(@PathVariable Long newDayEventId) {
		return newDayEventService.deleteNewDayEvent(newDayEventId);
	}
	
	@RequestMapping(value = RequestMapper.GET_NEW_EVENT_BY_ID, method = RequestMethod.GET)
	public String get(Model map, @PathVariable Long dayEventId) {
		NewDayEventDto newDayEventDto = newDayEventService.getNewDayEventById(dayEventId);
		map.addAttribute("dayEvent", newDayEventDto);  //change made by balu
		mapAttributesToModel(map);
		return "adddayevent";
	}
	
	@RequestMapping(value = RequestMapper.PUBLISH_NEW_DAY_EVENT, method = RequestMethod.GET)
	public @ResponseBody ResponseStatus publishNewDayEvent(@PathVariable Long dayEventId) {
		return newDayEventService.publishNewDayEvent(dayEventId);
	}
	
	private void mapAttributesToModel(Model map) {
		// Send list of categories to front end
		List<String> categories = Category.getList();
		
		map.addAttribute("categories", categories);
		
		// Send list of cities to front end
		List<String> cityList = new ArrayList<>();
		cityList.add("Nizamabad, Telangana, India");
		cityList.add("Hyderabad, Telangana, India");
		cityList.add("Mahabubnagar, Telangana, India");
		
		map.addAttribute("cities", cityList);
	}
}

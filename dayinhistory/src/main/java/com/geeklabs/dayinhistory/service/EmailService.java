package com.geeklabs.dayinhistory.service;

import com.geeklabs.dayinhistory.dto.UserDto;


public interface EmailService {
	
	void sendWelcomeMail(UserDto userDto);
	
	void send(String toEmail, String subject, String body);

	void sendForgotPasswordMail(UserDto userDto);
}

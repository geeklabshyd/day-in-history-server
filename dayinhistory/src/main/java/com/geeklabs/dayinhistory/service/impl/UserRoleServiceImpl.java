package com.geeklabs.dayinhistory.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.dayinhistory.domain.UserRole;
import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.geeklabs.dayinhistory.repository.UserRoleRepository;
import com.geeklabs.dayinhistory.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Override
	@Transactional(readOnly = true)
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		return userRoleRepository.getUserRoleByRoleName(roleName);
	}
}

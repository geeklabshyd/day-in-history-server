package com.geeklabs.dayinhistory.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.dayinhistory.converter.UserConverter;
import com.geeklabs.dayinhistory.domain.CustomUserDetails;
import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.domain.UserRole;
import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.geeklabs.dayinhistory.dto.ChangePwdDto;
import com.geeklabs.dayinhistory.dto.ForgotPasswordDto;
import com.geeklabs.dayinhistory.dto.UserDto;
import com.geeklabs.dayinhistory.repository.UserRepository;
import com.geeklabs.dayinhistory.repository.UserRoleRepository;
import com.geeklabs.dayinhistory.service.EmailService;
import com.geeklabs.dayinhistory.service.UserService;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserRoleRepository userRoleRepository;

	@Autowired
	private EmailService emailService;
	
	/*@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}*/

	@Override
	public ResponseStatus changePwd(ChangePwdDto changePwdDto, CustomUserDetails userDetails) {
		ResponseStatus status = new ResponseStatus();
		if (!changePwdDto.getConfirmPwd().equals(changePwdDto.getNewPwd())) {
			status.setStatus("mismatch");
			return status;
		}

		User user = userRepository.getUserByEmail(userDetails.getEmail());
		if (!user.getPassword().equals(changePwdDto.getPassword())) {
			status.setStatus("mismatch");
			return status;
		}

		user.setPassword(changePwdDto.getNewPwd());
		userRepository.save(user);
		status.setStatus("success");
		return status;
	}

	@Override
	public UserDetails loadUserByUsername(String userNameOrEmail) throws UsernameNotFoundException {
		User userByEamailOrUserName = getUserByEamailOrUserName(userNameOrEmail, userNameOrEmail);

		if (userByEamailOrUserName != null) {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			if (UserRoles.USER == userByEamailOrUserName.getUserRole()) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.USER.toString()));
			}

			String password = userByEamailOrUserName.getPassword();
			UserDetails user = new CustomUserDetails(userByEamailOrUserName.getUserName(), userByEamailOrUserName.getEmail(), password, authorities, userByEamailOrUserName.getId());
			return user;
		}
		return null;
	}

	@Override
	@Transactional
	public void setupAppUser() {
		// Create system admin
		UserDto userDto = new UserDto();
		userDto.setEmail("geeklabsapps@gmail.com");
		userDto.setFirstName("Geeek");
		userDto.setLastName("Labs");
		userDto.setUserName("geek");
		userDto.setPassword("geek");

		User user = UserConverter.convertUserDtoToUser(userDto);

		user.setUserRole(UserRoles.USER);
		// user.setUserStatus(UserStatus.ACTIVE);
		emailService.sendWelcomeMail(userDto);
//		user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));

		userRepository.save(user);

	}

	@Override
	@Transactional
	public void setupAppUserRoles() {

		UserRole userRole = new UserRole();
		// System Admin
		userRole.setUserRole(UserRoles.USER);
		userRoleRepository.save(userRole);

	}

	@Transactional(readOnly = true)
	@Override
	public User getUserByEamailOrUserName(String userName, String email) {
		return userRepository.getUserByEmailOrUserName(userName, email);
	}

	@Override
	public User getUserByName(String userName) {
		return userRepository.getUserByName(userName);
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseStatus sendforgotPasswordToUserMail(ForgotPasswordDto forgotPasswordDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		String userEmailOrUserName = forgotPasswordDto.getUserEmailOrUserName();

		if (userEmailOrUserName != null && !userEmailOrUserName.isEmpty()) {
			User user = userRepository.getUserByEmailOrUserName(userEmailOrUserName, userEmailOrUserName);
			try {
				emailService.sendForgotPasswordMail(UserConverter.convertUserToUserDto(user));
				responseStatus.setStatus("success");
			} catch (Exception exception) {

			}
		}
		return responseStatus;
	}

}

package com.geeklabs.dayinhistory.service.impl;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.geeklabs.dayinhistory.dto.UserDto;
import com.geeklabs.dayinhistory.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService {
	private static final String FROM_EMAIL = "geeklabsapps@gmail.com"; //geeklabsapps@gmail.com  maheshvelpula87@gmail.com
	@Autowired
	private TemplateEngine templateEngine;

	@Override
	public void sendWelcomeMail(UserDto userDto) {
		final Context ctx = new Context(); // TODO later define the locale for
											// the context when we do
											// internalization
		ctx.setVariable("user", userDto);
		final String htmlContent = this.templateEngine.process("user_credentials", ctx);

		send(userDto.getEmail(), "Welcome to the SMS and Call History", htmlContent);
	}

	@Override
	public void send(String toEmail, String subject, String body) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage msg = new MimeMessage(session);
		final MimeMessageHelper message = new MimeMessageHelper(msg);

		try {
			message.setFrom(new InternetAddress(FROM_EMAIL));
			InternetAddress to = new InternetAddress(toEmail);
			message.setTo(to);
			message.setSubject(subject);
			message.setText(body, true); //

			Transport.send(message.getMimeMessage());
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void sendForgotPasswordMail(UserDto userDto) {
		final Context ctx = new Context(); // TODO later define the locale for
		// the context when we do
		// internalization
		ctx.setVariable("user", userDto);
		final String htmlContent = this.templateEngine.process("user_forgot_password", ctx);
		send(userDto.getEmail(), "Forgot Password", htmlContent);
	}

}

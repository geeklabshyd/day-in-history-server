package com.geeklabs.dayinhistory.service;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.dayinhistory.domain.Document;


public interface BlobService {

	String storeBlob(String prefix, MultipartFile[] file) throws IOException;
	
	String storeBlob(String fileName, String mimeType, byte[] content) throws IOException;
	
	Document getFile(String fileName) throws IOException;
}

package com.geeklabs.dayinhistory.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.dayinhistory.converter.NewDayEventConverter;
import com.geeklabs.dayinhistory.converter.NewDayEventToDayEventConverter;
import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.domain.NewDayEvent;
import com.geeklabs.dayinhistory.dto.NewDayEventDto;
import com.geeklabs.dayinhistory.repository.NewDayEventRepository;
import com.geeklabs.dayinhistory.service.BlobService;
import com.geeklabs.dayinhistory.service.DayEventService;
import com.geeklabs.dayinhistory.service.NewDayEventService;
import com.geeklabs.dayinhistory.util.DateForMatter;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Service
public class NewDayEventServiceImpl implements NewDayEventService {

	@Autowired
	private NewDayEventRepository newDayEventRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private BlobService blobService;
	
	@Autowired 
	private DayEventService dayEventService;

	@Override
	@Transactional
	public ResponseStatus AddEvent(NewDayEventDto newDayEventDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		// check already exist or not
		NewDayEvent newDayEvent;

		newDayEvent = dozerBeanMapper.map(newDayEventDto, NewDayEvent.class);
		// set month, year , day
		/*
		 * DateForMatter dateForMatter = convertToDateFormat(adddayEventDto
		 * .getActualDate()); dayEvent.setDay(dateForMatter.getDay());
		 * dayEvent.setMonth(dateForMatter.getMonth());
		 * dayEvent.setYear(dateForMatter.getYear());
		 */

		newDayEvent.setCategoryName(newDayEventDto.getCategoryName());
		newDayEventRepository.save(newDayEvent);

		responseStatus.setStatus("success");
		return responseStatus;

	}

	private DateForMatter convertToDateFormat(String dateString) {
		DateForMatter dateForMatter = new DateForMatter();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = formatter.parse(dateString);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			dateForMatter.setFormattedDate(date);

			int year = calendar.getWeekYear();
			dateForMatter.setYear(year);
			int month = calendar.get(Calendar.MONTH);
			dateForMatter.setMonth(month);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			dateForMatter.setDay(day);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateForMatter;
	}

	public NewDayEvent isDayEventExist(String categoryName, String content) {
		return newDayEventRepository.isDayEventExist(categoryName, content);
	}

	@Override
	public ResponseStatus updateEvent(NewDayEventDto adddayEventDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<NewDayEventDto> getAllNewDayEventDtos() {
		List<NewDayEvent> newDayEvents = (List<NewDayEvent>) newDayEventRepository
				.findAll();
		List<NewDayEventDto> newDayEventDtos = NewDayEventConverter
				.convertNewDayEventsToNewDayEventDtos(newDayEvents, dozerBeanMapper);
		return newDayEventDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseStatus deleteNewDayEvent(Long newDayEventId) {
		ResponseStatus responseStatus = new ResponseStatus();
		newDayEventRepository.delete(newDayEventId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public NewDayEventDto getNewDayEventById(Long newDayEventId) {
		NewDayEvent newDayEvent = newDayEventRepository.findOne(newDayEventId);
		NewDayEventDto newDayEventDto = new NewDayEventDto();
		newDayEventDto = NewDayEventConverter.convertNewDayEventToNewDayEventDto(newDayEvent, newDayEventDto);
		return newDayEventDto;
	}
	
	
	@Override
	@Transactional
	public ResponseStatus publishNewDayEvent(Long dayEventId) {
		ResponseStatus responseStatus = new ResponseStatus();
		//Get New Event By Id
		NewDayEvent newDayEvent = newDayEventRepository.findOne(dayEventId);
		//convert new day event to Day Event
		DayEvent dayEvent = NewDayEventToDayEventConverter.convertNewDayEventToDayEvent(newDayEvent, dozerBeanMapper);
			// set month, year , day
			DateForMatter dateForMatter = convertToDateFormat(newDayEvent.getEventDate());
			dayEvent.setDay(dateForMatter.getDay());
			dayEvent.setMonth(dateForMatter.getMonth());
			dayEvent.setYear(dateForMatter.getYear());
				
			String[] places = newDayEvent.getCity().split(", ");
			dayEvent.setCity(places[0]);
			dayEvent.setState(places[1]);
			dayEvent.setCountry(places[2]);
		//save day event
		dayEventService.saveDayEvent(dayEvent);
		//delete new event
		newDayEventRepository.delete(dayEventId);
		responseStatus.setStatus("success");
		return responseStatus;
	}
	
	private DateForMatter convertToDateFormat(Date date) {
		DateForMatter dateForMatter = new DateForMatter();// Fri Jan 09 20:24:22 IST 2015
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		dateForMatter.setFormattedDate(date);

		int year = calendar.getWeekYear();
		dateForMatter.setYear(year);
		int month = calendar.get(Calendar.MONTH)+1;
		dateForMatter.setMonth(month);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		dateForMatter.setDay(day);
		return dateForMatter;
	}

}

package com.geeklabs.dayinhistory.service;

import java.util.List;

import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.dto.DayEventDto;
import com.geeklabs.dayinhistory.dto.InputDto;
import com.geeklabs.dayinhistory.util.ResponseStatus;

public interface DayEventService {

	ResponseStatus addEvent(DayEventDto dayEventDto);

	ResponseStatus updateEvent(DayEventDto dayEventDto);

	List<DayEventDto> getAllDayEventDtos();

	DayEventDto getDayEventById(Long dayEventId);

	ResponseStatus deleteDayEvent(Long dayEventId);

	List<DayEventDto> getAllDayEventsByPlaceCategoryAndDate(int currentPage, InputDto inputDto);
	
	ResponseStatus dayEventLikeCount(Long dayEventId);

	void saveDayEvent(DayEvent dayEvent);

}

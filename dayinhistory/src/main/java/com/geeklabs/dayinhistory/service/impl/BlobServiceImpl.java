package com.geeklabs.dayinhistory.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.dayinhistory.domain.Document;
import com.geeklabs.dayinhistory.service.BlobService;
import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

/**
 * MIME Type for JPG use "image/jpeg" for PNG use "image/png" PDF use
 * "application/pdf" see more: https://en.wikipedia.org/wiki/Internet_media_type
 */

// http://stackoverflow.com/questions/9671182/google-blobstore-versus-google-cloud-storage
// https://code.google.com/p/appengine-gcs-client/source/browse/trunk/java/example/src/com/google/appengine/demos/GcsExampleServlet.java
@Service
public class BlobServiceImpl implements BlobService {

	// private static final int BUFFER_SIZE = 3 * 1024 * 1024;

	AppIdentityService appIdentity = AppIdentityServiceFactory
			.getAppIdentityService();
	String bucketName = appIdentity.getDefaultGcsBucketName();
	/**
	 * This is where backoff parameters are configured. Here it is aggressively
	 * retrying with backoff, up to 10 times but taking no more that 15 seconds
	 * total to do so.
	 */
	private final GcsService gcsService = GcsServiceFactory
			.createGcsService(new RetryParams.Builder()
					.initialRetryDelayMillis(10).retryMaxAttempts(10)
					.totalRetryPeriodMillis(15000).build());

	@Override
	public Document getFile(String fileName) throws IOException {

		GcsFilename gcsFileName = getFileName(fileName);

		GcsFileMetadata metadata = gcsService.getMetadata(gcsFileName);
		if (metadata != null) {
			Document document = new Document();
			document.setMimeType(metadata.getOptions().getMimeType());
			document.setFileName(metadata.getFilename().getObjectName());
			
			int fileSize = (int) metadata.getLength();
			ByteBuffer result = ByteBuffer.allocate(fileSize);
			try (GcsInputChannel readChannel = gcsService.openReadChannel(
					gcsFileName, 0)) {
				readChannel.read(result);
			}
			document.setContent(result.array());
			return document;
		}
		return null;
	}

	
	@Override
	public String storeBlob(String prefix, MultipartFile[] files)throws IOException {
		String fileName = null;
		if (files != null) {
			for (MultipartFile file : files) {
				if (file.getSize() > 0) {
					fileName = prefix + UUID.randomUUID() + "_" + file.getOriginalFilename().replace(".", "_");
					storeBlob(fileName, file.getContentType(), file.getBytes());
				}
			}
		}
		return fileName;
	}

	@Override
	public String storeBlob(String fileName, String mimeType, byte[] content)
			throws IOException {
		GcsOutputChannel outputChannel;
		GcsFileOptions.Builder builder = new GcsFileOptions.Builder();
		// builder.addUserMetadata(key, value)
		builder.mimeType(mimeType);

		outputChannel = gcsService.createOrReplace(getFileName(fileName),
				builder.build());
		// Copy content to Cloud Storage
		copy(content, Channels.newOutputStream(outputChannel));

		return fileName;
	}
	
	private GcsFilename getFileName(String fileName) {
		return new GcsFilename("m-ateet.appspot.com", fileName);  //m-ateet.appspot.com , innate-bonfire-689.appspot.com
	}

	/**
	 * Transfer the data from the inputStream to the outputStream. Then close
	 * both streams.
	 */
	private void copy(byte[] content, OutputStream output) throws IOException {
		try {
			output.write(content);
		} finally {
			output.close();
		}
	}
}

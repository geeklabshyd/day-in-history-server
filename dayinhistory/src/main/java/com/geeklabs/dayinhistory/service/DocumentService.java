package com.geeklabs.dayinhistory.service;
import com.geeklabs.dayinhistory.domain.Document;

public interface DocumentService {
	Document getDocument(String identifier);
}
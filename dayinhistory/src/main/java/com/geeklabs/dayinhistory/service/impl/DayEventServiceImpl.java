package com.geeklabs.dayinhistory.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.dayinhistory.converter.DayEventConverter;
import com.geeklabs.dayinhistory.converter.NewDayEventToDayEventConverter;
import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.domain.NewDayEvent;
import com.geeklabs.dayinhistory.dto.DayEventDto;
import com.geeklabs.dayinhistory.dto.InputDto;
import com.geeklabs.dayinhistory.repository.DayEventRepository;
import com.geeklabs.dayinhistory.repository.NewDayEventRepository;
import com.geeklabs.dayinhistory.service.BlobService;
import com.geeklabs.dayinhistory.service.DayEventService;
import com.geeklabs.dayinhistory.util.DateForMatter;
import com.geeklabs.dayinhistory.util.Pageable;
import com.geeklabs.dayinhistory.util.ResponseStatus;

@Service
public class DayEventServiceImpl implements DayEventService {

	@Autowired
	private DayEventRepository dayEventRepository;
	
	@Autowired
	private NewDayEventRepository newDayEventRepository;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private BlobService blobService;

	@Override
	@Transactional
	public ResponseStatus addEvent(DayEventDto dayEventDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		// check already exist or not
		DayEvent dayEvent = isDayEventExist(dayEventDto.getCategoryName(),
				dayEventDto.getContent());
		if (dayEvent == null) {

			// image is there or not object
			MultipartFile[] dayEventImageFile = dayEventDto
					.getDayEventImageFile();
			if (dayEventImageFile != null) {
				saveDayEventImage(dayEventDto);
			}

			dayEvent = dozerBeanMapper.map(dayEventDto, DayEvent.class);
			// set month, year , day
			DateForMatter dateForMatter = convertToDateFormat(dayEventDto.getEventDate());
			dayEvent.setDay(dateForMatter.getDay());
			dayEvent.setMonth(dateForMatter.getMonth());
			dayEvent.setYear(dateForMatter.getYear());
			
			String[] places = dayEventDto.getCity().split(", ");
			if (places != null && places.length == 3) {
			dayEvent.setCity(places[0]);
			dayEvent.setState(places[1]);
			dayEvent.setCountry(places[2]);
			}else if (places != null && places.length == 2){
				dayEvent.setCity(null);
				dayEvent.setState(places[0]);
				dayEvent.setCountry(places[1]);
				
			}else if (places != null && places.length == 1){
				dayEvent.setCity(null);
				dayEvent.setState(null);
				dayEvent.setCountry(places[0]);
				
			}

			dayEvent.setCreatedDate(new Date());

			// set category id
			dayEvent.setCategoryName(dayEventDto.getCategoryName());
			dayEventRepository.save(dayEvent);

			responseStatus.setStatus("success");
			return responseStatus;
		}

		return responseStatus;
	}

	// handle delete blob ?
	private void saveDayEventImage(DayEventDto dayEventDto) {
		MultipartFile[] dayEventImageFile = dayEventDto.getDayEventImageFile();

		try {
			if (dayEventImageFile != null && dayEventImageFile.length > 0) {
				String dayEventImageFilePath = blobService.storeBlob(new Random().nextLong()+"_"+dayEventImageFile[0].getName(),
						dayEventImageFile);
				if (dayEventImageFilePath != null) {
					dayEventDto.setDayEventImagePath(dayEventImageFilePath);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DayEvent isDayEventExist(String categoryName, String content) {
		return dayEventRepository.isDayEventExist(categoryName, content);
	}
	@Override
	@Transactional(readOnly = true)
	public ResponseStatus dayEventLikeCount(Long dayEventId) {
		ResponseStatus responseStatus = new ResponseStatus();
		DayEvent dayEvent = dayEventRepository.findOne(dayEventId);
		dayEvent.setLikeCount(dayEvent.getLikeCount()+1);
		dayEventRepository.save(dayEvent);
		responseStatus.setStatus("success");
		return responseStatus;
	}
	@Override
	@Transactional
	public ResponseStatus updateEvent(DayEventDto dayEventDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		DayEvent dayEvent = dayEventRepository.findOne(dayEventDto.getId());
		if(dayEvent == null){
			NewDayEvent newDayEvent = newDayEventRepository.findOne(dayEventDto.getId());
			dayEvent = NewDayEventToDayEventConverter.convertNewDayEventToDayEvent(newDayEvent, dozerBeanMapper);
		}
		dayEvent = DayEventConverter.convertDayEventDtoToDayEvent(dayEventDto,
				dayEvent);
		// set month, year , day
		DateForMatter dateForMatter = convertToDateFormat(dayEventDto.getEventDate());
		dayEvent.setDay(dateForMatter.getDay());
		dayEvent.setMonth(dateForMatter.getMonth());
		dayEvent.setYear(dateForMatter.getYear());
					
		String[] places = dayEventDto.getCity().split(", ");
		if (places != null && places.length == 3) {
		dayEvent.setCity(places[0]);
		dayEvent.setState(places[1]);
		dayEvent.setCountry(places[2]);
		}else if (places != null && places.length == 2){
			dayEvent.setCity(null);
			dayEvent.setState(places[0]);
			dayEvent.setCountry(places[1]);
			
		}else if (places != null && places.length == 1){
			dayEvent.setCity(null);
			dayEvent.setState(null);
			dayEvent.setCountry(places[0]);
			
		}
		
		// image is there or not object
		MultipartFile[] dayEventImageFile = dayEventDto.getDayEventImageFile();
		if (dayEventImageFile != null) {
			saveDayEventImage(dayEventDto);
		}

		dayEvent.setDayEventImagePath(dayEventDto.getDayEventImagePath());
		
		dayEvent.setCreatedDate(new Date());
		// save updated dayevent
		dayEventRepository.save(dayEvent);

		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public List<DayEventDto> getAllDayEventDtos() {
		List<DayEvent> dayEvents = (List<DayEvent>) dayEventRepository
				.findAll();
		List<DayEventDto> dayEventDtos = DayEventConverter
				.convertDayEventsToDayEventDtos(dayEvents, dozerBeanMapper);
		return dayEventDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public DayEventDto getDayEventById(Long dayEventId) {
		DayEvent dayEvent = dayEventRepository.findOne(dayEventId);
		DayEventDto dayEventDto = new DayEventDto();
		dayEventDto = DayEventConverter.convertDayEventToDayEventDto(dayEvent,
				dayEventDto);
		dayEventDto.setCity(dayEvent.getCity() + ", " + dayEvent.getState() + ", " + dayEvent.getCountry());
		return dayEventDto;
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseStatus deleteDayEvent(Long dayEventId) {
		ResponseStatus responseStatus = new ResponseStatus();
		dayEventRepository.delete(dayEventId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional(readOnly = true)
	public List<DayEventDto> getAllDayEventsByPlaceCategoryAndDate(
			int currentPage, InputDto inputDto) {
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		DateForMatter dateFormat = convertToDateFormat(inputDto.getDate());
		List<DayEvent> dayEvents = dayEventRepository
				.getAllDayEventsByPlaceCategoryAndDate(pageable,
						inputDto.getCity(), dateFormat.getMonth(),
						dateFormat.getDay(), inputDto.getCategory());
		List<DayEventDto> dayEventDtos = DayEventConverter
				.convertDayEventsToDayEventDtos(dayEvents, dozerBeanMapper);
		return dayEventDtos;
	}

	private DateForMatter convertToDateFormat(Date date) {
		DateForMatter dateForMatter = new DateForMatter();// Fri Jan 09 20:24:22 IST 2015
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		dateForMatter.setFormattedDate(date);

		int year = calendar.getWeekYear();
		dateForMatter.setYear(year);
		int month = calendar.get(Calendar.MONTH)+1;
		dateForMatter.setMonth(month);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		dateForMatter.setDay(day);
		return dateForMatter;
	}

	private DateForMatter convertToDateFormat(String eventDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		try {
			Date date = format.parse(eventDate);
			
			DateForMatter dateForMatter = new DateForMatter();// Fri Jan 09 20:24:22 IST 2015
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			dateForMatter.setFormattedDate(date);

			int year = calendar.getWeekYear();
			dateForMatter.setYear(year);
			int month = calendar.get(Calendar.MONTH) +1;
			dateForMatter.setMonth(month);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			dateForMatter.setDay(day);
			return dateForMatter;
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	@Override
	@Transactional
	public void saveDayEvent(DayEvent dayEvent) {
		dayEventRepository.save(dayEvent);
	}
}

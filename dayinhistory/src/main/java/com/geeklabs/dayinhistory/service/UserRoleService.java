package com.geeklabs.dayinhistory.service;

import com.geeklabs.dayinhistory.domain.UserRole;
import com.geeklabs.dayinhistory.domain.enums.UserRoles;

public interface UserRoleService {

	UserRole getUserRoleByRoleName(UserRoles roles);

	
}

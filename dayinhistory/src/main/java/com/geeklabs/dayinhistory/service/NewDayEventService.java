package com.geeklabs.dayinhistory.service;

import java.util.List;

import com.geeklabs.dayinhistory.dto.NewDayEventDto;
import com.geeklabs.dayinhistory.util.ResponseStatus;

public interface NewDayEventService {

	ResponseStatus AddEvent(NewDayEventDto newDayEventDto);

	ResponseStatus updateEvent(NewDayEventDto newDayEventDto);

	List<NewDayEventDto> getAllNewDayEventDtos();

	ResponseStatus deleteNewDayEvent(Long newDayEventId);

	NewDayEventDto getNewDayEventById(Long newDayEventId);

	ResponseStatus publishNewDayEvent(Long dayEventId);

}

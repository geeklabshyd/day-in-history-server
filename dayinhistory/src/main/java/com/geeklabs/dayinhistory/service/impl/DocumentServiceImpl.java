package com.geeklabs.dayinhistory.service.impl;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geeklabs.dayinhistory.domain.Document;
import com.geeklabs.dayinhistory.service.BlobService;
import com.geeklabs.dayinhistory.service.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService {

	@Autowired
	private BlobService blobService;
	
	@Override
	public Document getDocument(String identifier) {
		try {
			return blobService.getFile(identifier);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
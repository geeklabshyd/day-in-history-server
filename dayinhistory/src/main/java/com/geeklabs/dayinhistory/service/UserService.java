package com.geeklabs.dayinhistory.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.geeklabs.dayinhistory.domain.CustomUserDetails;
import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.dto.ChangePwdDto;
import com.geeklabs.dayinhistory.dto.ForgotPasswordDto;
import com.geeklabs.dayinhistory.util.ResponseStatus;

public interface UserService extends UserDetailsService {

	void setupAppUserRoles();
	void setupAppUser();
	
	User getUserByEamailOrUserName(String userName, String email);
	User getUserByName(String userName);
	ResponseStatus changePwd(ChangePwdDto changePwdDto, CustomUserDetails userDetails);
	ResponseStatus sendforgotPasswordToUserMail(ForgotPasswordDto forgotPasswordDto);
	
}

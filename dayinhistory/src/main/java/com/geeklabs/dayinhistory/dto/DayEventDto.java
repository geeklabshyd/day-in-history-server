package com.geeklabs.dayinhistory.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class DayEventDto {

	private Long id;
	private String categoryName;
	private String city;
	private String state;
	private String country;
	private String content;
	private String dayEventImagePath;

	private String email;
	private String userName;

	private String eventDate;
	private Date createdDate;

	private int month;
	private int day;
	private int year;

	private int likeCount;
	private int weight;

	private MultipartFile[] dayEventImageFile;
	private boolean readOnly;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDayEventImagePath() {
		return dayEventImagePath;
	}

	public void setDayEventImagePath(String dayEventImagePath) {
		this.dayEventImagePath = dayEventImagePath;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public MultipartFile[] getDayEventImageFile() {
		return dayEventImageFile;
	}

	public void setDayEventImageFile(MultipartFile[] dayEventImageFile) {
		this.dayEventImageFile = dayEventImageFile;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}

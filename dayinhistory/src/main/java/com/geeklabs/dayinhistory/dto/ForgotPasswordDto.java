package com.geeklabs.dayinhistory.dto;

public class ForgotPasswordDto {

	private String userEmailOrUserName;
	
	public String getUserEmailOrUserName() {
		return userEmailOrUserName;
	}
	
	public void setUserEmailOrUserName(String userEmailOrUserName) {
		this.userEmailOrUserName = userEmailOrUserName;
	}
}

package com.geeklabs.dayinhistory.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class NewDayEventDto {

	private Long id;
	private String categoryName;
	private String city;
	private String content;
	private String userName;
	private String email;

	private Date createdDate;
	private Date eventDate;
	private String dayEventImagePath;

	private boolean readOnly;

	private int month;
	private int day;
	private int year;

	private int likeCount;
	private int weight;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public MultipartFile[] getDayEventImageFile() {
		return dayEventImageFile;
	}

	public void setDayEventImageFile(MultipartFile[] dayEventImageFile) {
		this.dayEventImageFile = dayEventImageFile;
	}

	private MultipartFile[] dayEventImageFile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getDayEventImagePath() {
		return dayEventImagePath;
	}

	public void setDayEventImagePath(String dayEventImagePath) {
		this.dayEventImagePath = dayEventImagePath;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

}

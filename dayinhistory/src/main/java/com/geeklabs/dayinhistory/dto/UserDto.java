package com.geeklabs.dayinhistory.dto;

public class UserDto {

	private Long id;
	private String email;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private String activationCode;
	private String picUrl;
	private String accessToken;
	private String status = "error";
	private boolean isUserRegistering;
	

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccesstoken() {
		return accessToken;
	}

	public void setAccesstoken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isUserRegistering() {
		return isUserRegistering;
	}

	public void setUserRegistering(boolean isUserRegistering) {
		this.isUserRegistering = isUserRegistering;
	}
	
}

package com.geeklabs.dayinhistory;

import java.io.Serializable;

public class Message implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	public static final String ERROR = "error";
	
	public static final String WARNING = "warning";
	
	public static final String SUCCESS = "success";
	
	private String message;
	
	private String type;
	
	private String displayMessage;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Message(){}
	
	public Message(String message,String type){
		this.message = message ;
		this.type = type;
		generateMessage();
	}
	
	protected void generateMessage(){		
		StringBuilder builder = new StringBuilder();		
		if(ERROR.equalsIgnoreCase(this.type)){
			builder.append("<div class=\"alert alert-danger\" style=\"margin:10px 0;\">");
			
		} else if(WARNING.equalsIgnoreCase(this.type)){
			builder.append("<div class=\"alert alert-warning\" style=\"margin:10px 0;\">");
			
		} else if(SUCCESS.equalsIgnoreCase(this.type)){
			builder.append("<div class=\"alert alert-success\" style=\"margin:10px 0;\">");
		}
		
		builder.append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>");
		builder.append(this.message);
		builder.append("</div>");
		this.displayMessage = builder.toString();
	}

	public String getDisplayMessage() {
		generateMessage();
		return displayMessage;
	}

	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}		
}

package com.geeklabs.dayinhistory.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.dayinhistory.domain.NewDayEvent;
import com.geeklabs.dayinhistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class NewDayEventRepositoryImpl extends AbstractObjectifyCRUDRepository<NewDayEvent> implements NewDayEventRepository {

	@Autowired 
	private Objectify objectify;
	

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public NewDayEventRepositoryImpl() {
		super(NewDayEvent.class);
	}

	@Override
	public NewDayEvent isDayEventExist(String categoryName, String content) {
		return objectify.load()
						.type(NewDayEvent.class)
						.filter("categoryName", categoryName)
						.filter("content", content)
						.first()
						.now();
	}
	
	/*@Override
	public List<DayEvent> getAllDayEventsByCategoryAndDate(Pageable pageable, int month, int day, String categoryName) {
		return objectify.load()
				.type(DayEvent.class)
				.filter("day", day)
				.filter("month", month)
				.filter("categoryName", categoryName)
				.offset(pageable.getOffset())
				.limit(Pageable.LIMIT)
				.list();
	}*/
}

package com.geeklabs.dayinhistory.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.domain.UserRole;
import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.geeklabs.dayinhistory.repository.UserRoleRepository;
import com.geeklabs.dayinhistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class UserRoleRepositoryImpl extends AbstractObjectifyCRUDRepository<UserRole> implements UserRoleRepository {
	@Autowired
	private Objectify objectify;
	
	public UserRoleRepositoryImpl() {
		super(UserRole.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public User getAdminByEmail(String email) {
		return objectify.load()
				.type(User.class)
				.filter("email", email)
				.first()
				.now();
	}

	@Override
	public UserRole getUserRoleByRoleName(UserRoles roleName) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
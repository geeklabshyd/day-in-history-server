package com.geeklabs.dayinhistory.repository;

import com.geeklabs.dayinhistory.domain.NewDayEvent;
import com.geeklabs.dayinhistory.repository.objectify.ObjectifyCRUDRepository;

public interface NewDayEventRepository extends ObjectifyCRUDRepository<NewDayEvent> {

	NewDayEvent isDayEventExist(String categoryName, String content);

	//List<DayEvent> getAllDayEventsByCategoryAndDate(Pageable pageable, int month, int day, String categoryName);

}

package com.geeklabs.dayinhistory.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.dayinhistory.util.Pageable;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.cmd.Query;

@Repository
public class DayEventRepositoryImpl extends AbstractObjectifyCRUDRepository<DayEvent> implements DayEventRepository {

	@Autowired 
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public DayEventRepositoryImpl() {
		super(DayEvent.class);
	}

	@Override
	public DayEvent isDayEventExist(String categoryName, String content) {
		return objectify.load()
						.type(DayEvent.class)
						.filter("categoryName", categoryName)
						.filter("content", content)
						.first()
						.now();
	}
	
	@Override
	public List<DayEvent> getAllDayEventsByPlaceCategoryAndDate(Pageable pageable, String place, int month, int day, String categoryName) {
		
		List<DayEvent> allEvents = new ArrayList<>();
		
		// No place got from client ??
		if (place == null || place.isEmpty() || "UnKnown".equals(place)) {
			return objectify.load()
					.type(DayEvent.class)
					.filter("day", day)
					.filter("month", month)
					.filter("categoryName", categoryName)
					.offset(pageable.getOffset())
					.limit(Pageable.LIMIT)
					.order("likeCount")
					.order("weight")
					.list();
		}
		
		String[] places = place.split(", ");
		
		// Get all events where city matched
		if (places != null && places.length == 3) {
			addRecordsByCity(pageable, month, day, categoryName, allEvents, places[0]);
			
			// Get all events where state matched but exclude city matched records
			addRecordsByState(pageable, month, day, categoryName, allEvents, places[1], places[0]);
			
			// Get country records
			addRecordsByCountry(pageable, month, day, categoryName, allEvents, places[2], places[1]);
		} else if (places != null && places.length == 2) {//state level
			// Get all events where state matched but exclude city matched records
			addRecordsByState(pageable, month, day, categoryName, allEvents, places[0], null);
			
			// Get country records
			addRecordsByCountry(pageable, month, day, categoryName, allEvents, places[1], places[0]);
		} else if (places != null && places.length == 1) {//only country
			// Get country records
			addRecordsByCountry(pageable, month, day, categoryName, allEvents, places[0], null);
		}
		
		return allEvents;
	}

	private void addRecordsByCountry(Pageable pageable, int month, int day,
			String categoryName, List<DayEvent> allEvents, String country, String state) {
		// Get all events where country matched but exclude state matched records.
		List<DayEvent> countryMatchedEvents = new ArrayList<>();
				
		Query<DayEvent> filter = objectify.load()
				.type(DayEvent.class)
				.filter("day", day)
				.filter("month", month)
				.filter("categoryName", categoryName)
				.filter("country", country);
		
				if (state !=null) {
					countryMatchedEvents = filter.filter("state !=", state)
							.offset(pageable.getOffset())
							.limit(Pageable.LIMIT)
							.order("state")
							.order("likeCount")
							.order("weight")
							.list();
				} else {
					countryMatchedEvents = filter.offset(pageable.getOffset())
							.limit(Pageable.LIMIT)
							.order("likeCount")
							.order("weight")
							.list();
				}
				if (countryMatchedEvents != null && !countryMatchedEvents.isEmpty()) {
						allEvents.addAll(countryMatchedEvents);
				}
	}

	private void addRecordsByState(Pageable pageable, int month, int day,
			String categoryName, List<DayEvent> allEvents, String state, String city) {
		List<DayEvent> stateMatchedEvents =new ArrayList<>();
		
		Query<DayEvent> filter = objectify.load()
				.type(DayEvent.class)
				.filter("day", day)
				.filter("month", month)
				.filter("categoryName", categoryName)
				.filter("state", state);
		if (city != null) {
			stateMatchedEvents = filter.filter("city !=", city)
				.offset(pageable.getOffset())
				.limit(Pageable.LIMIT)
				.order("city")
				.order("likeCount")
				.order("weight")
				.list();
		} else {
			stateMatchedEvents = filter.offset(pageable.getOffset())
					.limit(Pageable.LIMIT)
					.order("likeCount")
					.order("weight")
					.list();
		}
		
		if (stateMatchedEvents != null && !stateMatchedEvents.isEmpty()) {
			allEvents.addAll(stateMatchedEvents);
		}
	}

	private void addRecordsByCity(Pageable pageable, int month, int day,
			String categoryName, List<DayEvent> allEvents, String city) {
		List<DayEvent> cityMatchedEvents = objectify.load()
				.type(DayEvent.class)
				.filter("day", day)
				.filter("month", month)
				.filter("categoryName", categoryName)
				.filter("city", city)
				.offset(pageable.getOffset())
				.limit(Pageable.LIMIT)
				.order("likeCount")
				.order("weight")
				.list();
		if (cityMatchedEvents != null && !cityMatchedEvents.isEmpty()) {
			allEvents.addAll(cityMatchedEvents);
		}
		
	}

	@Override
	public DayEvent dayEventLikeCount(int likeCount) {
		return objectify.load()
				.type(DayEvent.class)
				.filter("likeCount", likeCount)
				.first()
				.now();
	}
}

package com.geeklabs.dayinhistory.repository;

import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User>{

	User getUserByEmailOrUserName(String userName, String email);
	User getUserByName(String userName);
	User getUserByEmail(String email);
}

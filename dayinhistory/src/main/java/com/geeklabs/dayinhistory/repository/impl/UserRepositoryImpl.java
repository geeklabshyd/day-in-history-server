package com.geeklabs.dayinhistory.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.repository.UserRepository;
import com.geeklabs.dayinhistory.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class UserRepositoryImpl extends AbstractObjectifyCRUDRepository<User> implements UserRepository {
	
	
	@Autowired
	private Objectify objectify;
	
	public UserRepositoryImpl() {
		super(User.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	@Override
	public User getUserByEmailOrUserName(String userName, String email) {
		
		if(userName.contains("@") || email.contains("@")){
			return objectify.load()
					.type(User.class)
					.filter("email", email)
					.first()
					.now();
		}else {
			return objectify.load()
					.type(User.class)
					.filter("userName", email)
					.first()
					.now();
			
		}
		
	}

	@Override
	public User getUserByName(String userName) {
		return null;
	}
	
	@Override
	public User getUserByEmail(String email) {
		return objectify.load()
				.type(User.class)
				.filter("email", email)
				.first()
				.now();
	}
}

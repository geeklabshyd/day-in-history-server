package com.geeklabs.dayinhistory.repository;

import com.geeklabs.dayinhistory.domain.UserRole;
import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.geeklabs.dayinhistory.repository.objectify.ObjectifyCRUDRepository;

public interface UserRoleRepository extends ObjectifyCRUDRepository<UserRole> {
	
	UserRole getUserRoleByRoleName(UserRoles roleName);
}

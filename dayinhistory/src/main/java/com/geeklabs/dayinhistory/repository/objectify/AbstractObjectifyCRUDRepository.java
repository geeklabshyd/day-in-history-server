package com.geeklabs.dayinhistory.repository.objectify;

import java.util.Map;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

public abstract class AbstractObjectifyCRUDRepository<T> implements ObjectifyCRUDRepository<T> {

	private Class<T> entityAggregateRootObjectifyClass;
	
	public AbstractObjectifyCRUDRepository() {
	}
	
	protected AbstractObjectifyCRUDRepository(Class<T> entityAggregateRootObjectifyClass) {
		this.entityAggregateRootObjectifyClass = entityAggregateRootObjectifyClass;
	}

	@Override
	public <S extends T> Key<S> save(S entity) {
		return getObjectify().save().entity(entity).now();
	}

	@Override
	public <S extends T> Map<Key<S>, S> save(Iterable<S> entities) {
		return getObjectify().save().entities(entities).now();
	}

	@Override
	public T findOne(Long id) {
		return getObjectify().load().type(entityAggregateRootObjectifyClass).id(id).now();
	}

	@Override
	public Iterable<T> findAll() {
		return getObjectify().load().type(entityAggregateRootObjectifyClass).list();
	}

	@Override
	public long count() {
		return getObjectify().load().type(entityAggregateRootObjectifyClass).count();
	}

	@Override
	public void delete(Long id) {
		getObjectify().delete().type(entityAggregateRootObjectifyClass).id(id);
	}

	@Override
	public void delete(T entity) {
		getObjectify().delete().entity(entity);
	}

	@Override
	public void delete(Iterable<? extends T> entities) {
		getObjectify().delete().entities(entities);
	}

	protected abstract Objectify getObjectify();
}

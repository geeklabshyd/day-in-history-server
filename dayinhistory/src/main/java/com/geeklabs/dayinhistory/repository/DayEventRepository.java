package com.geeklabs.dayinhistory.repository;

import java.util.List;

import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.repository.objectify.ObjectifyCRUDRepository;
import com.geeklabs.dayinhistory.util.Pageable;

public interface DayEventRepository extends ObjectifyCRUDRepository<DayEvent> {

	DayEvent isDayEventExist(String categoryName, String content);

	List<DayEvent> getAllDayEventsByPlaceCategoryAndDate(Pageable pageable, String place, int month, int day, String categoryName);
	
	DayEvent dayEventLikeCount(int likeCount);

}

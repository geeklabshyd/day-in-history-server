package com.geeklabs.dayinhistory.util;

public class RequestMapper {
	
	public final static String USER = "user";

	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	public static final String IS_ACTIVATED = "/activate";
	
	/** Document **/
	public static final String DOCUMENT = "/doc";
	public static final String GET_DOCUMENT = "/get/{identifier}";
	
	/**Admin**/
	public static final String ADMIN = "/admin";
	public static final String SETUP = "/admin/setup";
	
	/**AUTHENTICATION**/
	public static final String AUTHENTICATION = "/authentication";
	public static final String SIGNIN = "/signIn";
	public static final String SIGNOUT = "/signout/{userId}";
	
	/** User **/
	public static final String MY_ACCOUNT = "/myAccount";
	public static final String MY_ACCOUNT_GET_USER = "/myAccount/getUserInfo";
	/**Forgot Password**/
	public static final String USER_FORGOTPASSWORD = "/forgotPassword";


	/**Category**/
	public static final String OPEN_CATEGORY_PAGE = "/category/list";
	public static final String ADD_CATEGORY = "/category/add";
	public static final String GET_ALL_CATEGORIES = "/category/list/get";
	public static final String GET_CATEGORY_BY_ID = null;
	public static final String UPDATE_CATEGORY = "/category/update";
	public static final String DELETE_CATEGORY = "/category/delete/{categoryId}";
	
	
	/** Day Event **/
	public static final String OPEN_DAY_EVENT = "/dayevent";   
	public static final String ADD_DAY_EVENT = "/dayevent/add";
	public static final String GET_ALL_DAY_EVENTS = "/dayevent/list/get";
	public static final String GET_EVENT_BY_ID = "/dayevent/update/{dayEventId}";
	public static final String DELETE_DAY_EVENT = "/dayevent/delete/{dayEventId}";
	public static final String GET_DAY_EVENTS_BY_DATE_AND_CATEGORY = "/dayevent/get";
	public static final String DAY_EVENT_LIKE_COUNT = "/dayevent/likecount/{dayEventID}";

	/**New Day Events**/
	public static final String OPEN_NEW_DAY_EVENT = "/newdayevent";
	public static final String ADD_DAY_EVENT_FROM_USER = "/dayevent/addfromuser";
	public static final String GET_ALL_NEW_DAY_EVENTS = "/newdayevent/list/get";
	public static final String DELETE_NEW_DAY_EVENT = "/newdayevent/delete/{newDayEventId}";
	public static final String GET_NEW_EVENT_BY_ID = "/newdayevent/update/{dayEventId}";
	public static final String PUBLISH_NEW_DAY_EVENT = "/newdayevent/publish/{dayEventId}";
	

}

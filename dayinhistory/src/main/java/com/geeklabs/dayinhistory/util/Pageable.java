package com.geeklabs.dayinhistory.util;

public class Pageable {
	public static final int LIMIT = 50;
	private int offset;
	
	public int getOffset() {
		return offset * LIMIT;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}	
}
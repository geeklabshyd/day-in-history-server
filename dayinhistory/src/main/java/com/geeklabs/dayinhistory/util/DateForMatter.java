package com.geeklabs.dayinhistory.util;

import java.util.Date;

public class DateForMatter {

	private int month;
	private int day;
	private int year;
	private Date formattedDate;
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Date getFormattedDate() {
		return formattedDate;
	}
	public void setFormattedDate(Date formattedDate) {
		this.formattedDate = formattedDate;
	}
}

package com.geeklabs.dayinhistory.domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Version;

import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.google.common.base.Objects;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Index
	private String email;
	private String firstName;
	private String lastName;
	@Index
	private String userName;
	@Index
	private String password;
	private long phone;
	private String activationCode;
	@Version
	private int optLockVersion;
	private boolean signIn;
	@Index
	@Enumerated(EnumType.STRING)
	private UserRoles userRole;
	private String picUrl;
	
	public String getPicUrl() {
		return picUrl;
	}
	
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	
	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public int getOptLockVersion() {
		return optLockVersion;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public long getPhone() {
		return phone;
	}
	public boolean isSignIn() {
		return signIn;
	}
	
	public void setSignIn(boolean signIn) {
		this.signIn = signIn;
	}
	public UserRoles getUserRole() {
		return userRole;
	}
	
	public void setUserRole(UserRoles userRole) {
		this.userRole = userRole;
	}
	

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User user = (User) obj;
		return Objects.equal(this.id, user.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}
}

package com.geeklabs.dayinhistory.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Version;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *  Custom UserDetails in order to keep the custom information of user
 *  into the security context to access easily through out the application. 
 *  Minimize the Database calls to access the user object.
 *  
 *  Currently there is only User Identifier of application is kept in the
 *  Domain. Adjust as per the requirement.  
 *   
 * */
public class CustomUserDetails implements UserDetails, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String password;
	private String username;
	private String email;
	private boolean accountNonExpired;
	private boolean accountNonLocked;
	private boolean credentialsNonExpired;
	private boolean enabled;
	private String userRole;
	List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {		
		return this.authorities;
	}

	@Version
	private int optLockVersion;
	
	public int getOptLockVersion() {
		return optLockVersion;
	}
	
	@Override
	public String getPassword() {		
		return this.password;
	}

	@Override
	public String getUsername() {		
		return this.username;
	}

	public String getEmail() {
		return email;
	}
	
	@Override
	public boolean isAccountNonExpired() {		
		return this.accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {		
		return this.accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {		
		return this.credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
	
	public CustomUserDetails(String username, String email, String password, List<GrantedAuthority> authorities){
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
		
		this.enabled = true;
		this.accountNonExpired = true;
		this.accountNonLocked = true;
		this.credentialsNonExpired = true;
	}
	
	public CustomUserDetails(String username, String email, String password, List<GrantedAuthority> authorities, Long id){
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
		this.id = id;
		
		this.enabled = true;
		this.accountNonExpired = true;
		this.accountNonLocked = true;
		this.credentialsNonExpired = true;
	}
	
	
	public CustomUserDetails(String username, String email, String password, List<GrantedAuthority> authorities, Long id, String userRole){
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
		this.id = id;
		
		this.enabled = true;
		this.accountNonExpired = true;
		this.accountNonLocked = true;
		this.credentialsNonExpired = true;
		
		this.userRole = userRole;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}	
	
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getUserRole() {
		return userRole;
	}
}
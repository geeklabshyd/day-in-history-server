package com.geeklabs.dayinhistory.domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Version;

import com.geeklabs.dayinhistory.domain.enums.UserRoles;
import com.google.common.base.Objects;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class UserRole {

	@Id
	@GeneratedValue
	private Long id;

	@Enumerated(EnumType.STRING)
	private UserRoles userRole;

	@Version
	private int optLockVersion;

	public int getOptLockVersion() {
		return optLockVersion;
	}

	public UserRoles getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRoles userRole) {
		this.userRole = userRole;
	}

	public Long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRole userRole = (UserRole) obj;
		return Objects.equal(this.id, userRole.getId());
	}
}

package com.geeklabs.dayinhistory.domain.enums;

public enum UserStatus {
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}

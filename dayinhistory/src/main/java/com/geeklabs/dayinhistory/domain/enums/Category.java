package com.geeklabs.dayinhistory.domain.enums;

import java.util.ArrayList;
import java.util.List;

public enum Category {

	SCIENCE("Science"), TECHNOLOGY("Technology"), SPORTS("Sports"), FILMS("Films"), POLITICS("Politics"), BIRTHDAYS("Birthdays"), WARS("Wars"), GENERAL("General"), DEATHS("Deaths") ;
	
	
	
	
	private String name;
	
	private Category(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static List<String> getList() {
		List<String> list = new ArrayList<>();
		for (Category cat : values()) {
			list.add(cat.getName());
		}
		return list;
	}
}

package com.geeklabs.dayinhistory.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.dto.DayEventDto;

public class DayEventConverter {

	public static DayEvent convertDayEventDtoToDayEvent(DayEventDto dayEventDto, DayEvent dayEvent) {
		dayEvent.setEventDate(dayEventDto.getEventDate());
		dayEvent.setCategoryName(dayEventDto.getCategoryName());
		dayEvent.setCity(dayEventDto.getCity());
		dayEvent.setContent(dayEventDto.getContent());
		dayEvent.setDayEventImagePath(dayEventDto.getDayEventImagePath());
		dayEvent.setEmail(dayEventDto.getEmail());
		dayEvent.setUserName(dayEventDto.getUserName());
//		dayEvent.setId(dayEventDto.getId());
		return dayEvent;
	}

	public static List<DayEventDto> convertDayEventsToDayEventDtos(List<DayEvent> dayEvents, DozerBeanMapper dozerBeanMapper) {
		ArrayList<DayEventDto> dayEventDtos = new ArrayList<>();
		//FIXME REGIONS
		for (DayEvent dayEvent : dayEvents) {
			DayEventDto dayEventDto = dozerBeanMapper.map(dayEvent, DayEventDto.class);
			dayEventDtos.add(dayEventDto);
		}
		return dayEventDtos;
	}

	public static DayEventDto convertDayEventToDayEventDto(DayEvent dayEvent, DayEventDto dayEventDto) {
			dayEventDto.setEventDate(dayEvent.getEventDate());
			dayEventDto.setCategoryName(dayEvent.getCategoryName());
			//FIXME regions And Imagepath
			dayEventDto.setCity(dayEvent.getCity());
			dayEventDto.setContent(dayEvent.getContent());
			dayEventDto.setDayEventImagePath(dayEvent.getDayEventImagePath());
			dayEventDto.setEmail(dayEvent.getEmail());
			dayEventDto.setUserName(dayEvent.getUserName());
			dayEventDto.setId(dayEvent.getId());
		return dayEventDto;
	}
}

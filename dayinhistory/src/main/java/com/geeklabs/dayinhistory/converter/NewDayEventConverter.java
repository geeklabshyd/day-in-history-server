package com.geeklabs.dayinhistory.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.dayinhistory.domain.NewDayEvent;
import com.geeklabs.dayinhistory.dto.NewDayEventDto;

public class NewDayEventConverter {

	public static NewDayEvent convertNewDayEventDtoToNewDayEvent(NewDayEventDto newDayEventDto, NewDayEvent newDayEvent) {
		newDayEvent.setEventDate(newDayEventDto.getEventDate());
		newDayEvent.setCategoryName(newDayEventDto.getCategoryName());
		newDayEvent.setContent(newDayEventDto.getContent());
		newDayEvent.setEmail(newDayEventDto.getEmail());
		newDayEvent.setUserName(newDayEventDto.getUserName());
		newDayEvent.setId(newDayEventDto.getId());
		newDayEvent.setCity(newDayEventDto.getCity());
		newDayEvent.setWeight(newDayEventDto.getWeight());
		
		return newDayEvent;
	}

	public static List<NewDayEventDto> convertNewDayEventsToNewDayEventDtos(List<NewDayEvent> newDayEvents, DozerBeanMapper dozerBeanMapper) {
		ArrayList<NewDayEventDto> newDayEventDtos = new ArrayList<>();
		//FIXME REGIONS
		for (NewDayEvent newDayEvent : newDayEvents) {
			NewDayEventDto newDayEventDto = dozerBeanMapper.map(newDayEvent, NewDayEventDto.class);
			newDayEventDtos.add(newDayEventDto);
		}
		return newDayEventDtos;
	}

	public static NewDayEventDto convertNewDayEventToNewDayEventDto(NewDayEvent newDayEvent, NewDayEventDto newDayEventDto) {
			newDayEventDto.setEventDate(newDayEvent.getEventDate());
			
			newDayEventDto.setCategoryName(newDayEvent.getCategoryName());
			newDayEventDto.setCity(newDayEvent.getCity());
			//FIXME regions And Image path
			newDayEventDto.setContent(newDayEvent.getContent());
			newDayEventDto.setWeight(newDayEvent.getWeight());
			newDayEventDto.setEmail(newDayEvent.getEmail());
			newDayEventDto.setUserName(newDayEvent.getUserName());
			newDayEventDto.setId(newDayEvent.getId());
		return newDayEventDto;
	}
}

package com.geeklabs.dayinhistory.converter;

import com.geeklabs.dayinhistory.domain.User;
import com.geeklabs.dayinhistory.dto.UserDto;

public class UserConverter {

	public static UserDto convertUserToUserDto(User user) {
		
		UserDto userDto = new UserDto();
		userDto.setEmail(user.getEmail());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setPassword(user.getPassword());
		userDto.setUserName(user.getUserName());
		userDto.setActivationCode(user.getActivationCode());
		return userDto;
	}
	
	public static User convertUserDtoToUser(UserDto userDto) {
		
		User user = new User();
		user.setEmail(userDto.getEmail());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setPassword(userDto.getPassword());
		user.setUserName(userDto.getUserName());
		user.setActivationCode(userDto.getActivationCode());
		return user;
	}
}

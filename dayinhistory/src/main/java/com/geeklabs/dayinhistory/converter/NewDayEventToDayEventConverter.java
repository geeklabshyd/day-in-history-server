package com.geeklabs.dayinhistory.converter;

import org.dozer.DozerBeanMapper;

import com.geeklabs.dayinhistory.domain.DayEvent;
import com.geeklabs.dayinhistory.domain.NewDayEvent;

public class NewDayEventToDayEventConverter {

	public static DayEvent convertNewDayEventToDayEvent(NewDayEvent newDayEvent, DozerBeanMapper dozerBeanMapper) {
		DayEvent dayEvent = dozerBeanMapper.map(newDayEvent, DayEvent.class);
		return dayEvent;
	}
}
